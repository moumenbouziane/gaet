#!/bin/sh

set -o errexit
set -o nounset
set -o xtrace

fetch(){
	mkdir -p /usr/local/$2
	TMP=$(mktemp)
	wget $1 --quiet --output-document ${TMP}
	tar xf ${TMP} --directory /usr/local/$2 --strip-components=1
	rm ${TMP}
}


BUILD_DEPENDENCIES="wget ca-certificates patch"
RUNTIME_DEPENDENCIES="\
	bioperl \
	file \
	less \
	libdatetime-perl \
	libdigest-md5-perl \
	libidn11 \
	libxml-simple-perl \
	openjdk-7-jre-headless"

apt-get update
apt-get install --yes --no-install-recommends ${BUILD_DEPENDENCIES}

# hmmer3
fetch 'http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz' hmmer3


# barrnap
fetch https://github.com/tseemann/barrnap/archive/0.7.tar.gz barrnap
ln -s /usr/local/barrnap/binaries/linux/* /usr/local/barrnap/bin/* /usr/local/bin

# Patch with JGI changes to barrnap containing RNaseP and SRP
cd /usr/local/barrnap
wget --output-document - \
	'https://patch-diff.githubusercontent.com/raw/tseemann/barrnap/pull/18.diff' | patch -p1
cd ./build
PATH=/usr/local/hmmer3/binaries:${PATH} ./build_HMMs.sh
mv arc.hmm bac.hmm euk.hmm mito.hmm ../db
rm -rf /usr/local/barrnap/binaries/darwin /usr/local/barrnap/examples /usr/local/barrnap/build /usr/local/hmmer3


# prokka
COMMIT=1caf2394850998f89a3782cc8846dc51978faac2
fetch https://github.com/tseemann/prokka/archive/${COMMIT}.tar.gz prokka
ln -s /usr/local/prokka/binaries/linux/* /usr/local/prokka/bin/* /usr/local/bin
rm -rf /usr/local/prokka/binaries/darwin /usr/local/prokka/doc


# tbl2asn
mkdir -p /usr/local/tbl2asn/bin
wget ftp://ftp.ncbi.nih.gov/toolbox/ncbi_tools/converters/by_program/tbl2asn/linux64.tbl2asn.gz \
	--quiet --output-document - \
	| gunzip > /usr/local/tbl2asn/bin/tbl2asn
chmod 700 /usr/local/tbl2asn/bin/tbl2asn
rm /usr/local/bin/tbl2asn
ln -s /usr/local/tbl2asn/bin/tbl2asn /usr/local/bin


# Clean up dependencies
apt-get autoremove --purge --yes ${BUILD_DEPENDENCIES}
apt-get clean

# Ensure RUNTIME_DEPENDENCIES are installed after purging BUILD_DEPENDENCIES
apt-get install --yes --no-install-recommends ${RUNTIME_DEPENDENCIES}
rm -rf /var/lib/apt/lists/*

# Build prokka database
prokka --setupdb
