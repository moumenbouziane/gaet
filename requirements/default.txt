funcy        >= 1.7.1,   < 1.8.0
gffutils     >= 0.8.7.1, < 0.9.0.0
pyfaidx      >= 0.4.8.1, < 0.5.0.0
ruamel.yaml  >= 0.11.14, < 0.12.0
