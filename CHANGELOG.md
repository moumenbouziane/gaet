# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this
project adheres to [Semantic Versioning](http://semver.org/).

## 0.3.0 - 2017-03-21

### Changed

  * Annotations not of the type (CDS/tRNA/tmRNA/rRNA/repeat_region) are
    excluded from the estimation of metrics. This is to prevent duplicate
    `gene` entries inflating the size metrics calculated, and also to prevent
    unknown gene types possibly changing the output in unexpected ways.

  * Renamed calculated metric groups to be consistent, where gene_copy and
    gene_type are consistent prefixes in the output YAML.

  * Moved SRP and RNaseP genes to a separate 'single_copy' metric group as they
    are no ribosomal rRNA. This is to prevent any possible confusion about the
    the rRNA metric group.

### Added

  * Add size metrics for each of the genes listed in the gene_types.yml file.
    The output now includes size metrics for both groups of genes and
    individual groups of gene producing the same gene product. An example of
    all genes of the same `gene_type` would be all rRNA genes, and genes of the
    same `gene_copy` would be all genes with `16S rRNA` as the product.

  * All metrics are additionally calculated for tmRNA genes.

  * Added L1/L2 norms to the gene distance metrics calculated when a reference
    GFF is supplied. These are the Manhattan and Euclidean distances
    respectively. These metrics are calculated for both the gene types and gene
    copy groups.

  * Flag --output/-o to specify location of output metrics.

### Fixed

  * Handle cases where `product` field is not specified in the GFF. In these
    cases the product field is set to the default of 'hypothetical protein`.

## 0.2.0 - 2016-12-15

### Added

  * Implemented calculation of gene set comparison, unique gene overlap, ng50
    and lg50 and gene count comparison metrics.

## 0.1.0 - 2016-11-17

### Added

  * Initial release of command line tool to evaluate assemblies using the GFF
    annotations.

