require 'aruba/aruba_path'
require 'yaml'
require 'json'

def last_json
  JSON.dump(@document)
end

Then(/^the file "([^"]*)" should be a valid YAML document$/) do |file|
  Aruba.configure do |config|
    @path = File.join(config.working_directory, file)
  end

  expect do
    @document = YAML.load(File.read(@path))
  end.to_not raise_error
end
