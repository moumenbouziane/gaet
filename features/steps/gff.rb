Given(/^I create a "([^"]*)" gff entry file "([^"]*)"$/) do |gff, dst|
  type, name = gff.split("/")
  src = `python test/zoo_helper.py #{type} #{name}`.strip
  FileUtils.mv src, "tmp/aruba/" + dst
end
