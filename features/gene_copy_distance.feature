Feature: Assessing the quality of an assembly using gene set distance comparisions

  Scenario Outline: Computing gene copy distance metrics
    Given I create a "<assembly>" gff entry file "assembly.gff"
      And I create a "<reference>" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_copy_distance/<group>" should have the following:
         | n_intersect               | <n_intr>  |
         | n_assembly_only           | <n_asm>   |
         | n_reference_only          | <n_ref>   |
         | n_symmetric_difference    | <n_symm>  |
         | perc_intersect            | <p_intr>  |
         | perc_assembly_only        | <p_asm>   |
         | perc_reference_only       | <p_ref>   |
         | perc_symmetric_difference | <p_symm>  |
         | perc_symmetric_difference | <p_symm>  |
         | l1_norm                   | <l1>      |
         | l2_norm                   | <l2>      |

    Examples:
      | assembly             | reference            | group    | n_intr | n_asm | n_ref | n_symm | p_intr | p_asm | p_ref | p_symm | l1 | l2    |
      | trna/two_alanine_ggc | trna/two_alanine_ggc | ala_tRNA | 1      | 0     | 0     | 0      | 1.0    | 0.0   | 0.0   | 0.0    | 0  | 0.0   |
      | trna/two_alanine_ggc | trna/alanine_ggc     | ala_tRNA | 1      | 0     | 0     | 0      | 1.0    | 0.0   | 0.0   | 0.0    | 1  | 1.0   |
