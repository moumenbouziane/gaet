Feature: Handling user errors

  Scenario: Getting command line help
    When I run `gaet -h`
    Then the exit status should be 0
     And the stderr should not contain anything
     And the stdout should contain:
     """
     A gene-based genome assembly evaluation tool
     """


  Scenario: Running gaet on a file that does not exist
    When I run `gaet non-existent-file`
     And the stdout should not contain anything
     And the stderr should contain:
     """
     File does not exist: 'non-existent-file'
     """
     And the exit status should be 1


  Scenario: Running gaet with a reference file that does not exist
    Given I create a "cds/two" gff entry file "assembly.gff"
    When I run `gaet --reference=non-existent-file assembly.gff`
     And the stdout should not contain anything
     And the stderr should contain:
     """
     File does not exist: 'non-existent-file'
     """
     And the exit status should be 1


  Scenario: Running gaet on an GFF file containing only comments
    Given the file "empty.gff" with:
      """
      ##gff-version 3
      """
     When I run `gaet empty.gff`
      And the stdout should not contain anything
      And the stderr should contain:
      """
      File appears to have no annotations: 'empty.gff'
      """
      And the exit status should be 1

  Scenario: Running gaet on an GFF which does not contain ##FASTA
    Given the file "no_fasta.gff" with:
      """
      ##gff-version 3
      Chromosome	test_data	rRNA	1	60	.	-	.	ID=gene_1;product=5S ribosomal RNA
      """
     When I run `gaet no_fasta.gff`
      And the stdout should not contain anything
      And the stderr should contain:
      """
      GFF file appears to have no FASTA entry: 'no_fasta.gff'
      """
      And the exit status should be 1


  Scenario: Running gaet on an GFF file containing only blank lines
    Given the file "empty.gff" with:
      """
      ##gff-version 3

      """
     When I run `gaet empty.gff`
      And the stdout should not contain anything
      And the stderr should contain:
      """
      File appears to have no annotations: 'empty.gff'
      """
      And the exit status should be 1
