Feature: Using gaet with variations in the input data

  Scenario: Computing gene set distance metrics with reverse complement
    Given I create a "trna/alanine_ggc" gff entry file "assembly.gff"
      And I create a "trna/alanine_ggc_rev_comp" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_type_distance/tRNA" should have the following:
         | n_intersect               | 1 |
         | n_assembly_only           | 0 |
         | n_reference_only          | 0 |
         | n_symmetric_difference    | 0 |


  Scenario: Computing gene set distance metrics with a line break
    Given I create a "trna/alanine_ggc" gff entry file "assembly.gff"
      And the file "reference.gff" with:
      """
      ##gff-version 3
      ##sequence-region Chromosome 1 60
      Chromosome	test_data	tRNA	1	60	.	+	.	ID=tRNA_1;product=tRNA-Ala(ggc)
      ##FASTA
      > Chromosome
      GGACAACACACTAACTCTGAAGCTGAGGTTCGA
      CCACAATTACAACGTTATAGAAATCAA
      """
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_type_distance/tRNA" should have the following:
         | n_intersect               | 1 |
         | n_assembly_only           | 0 |
         | n_reference_only          | 0 |
         | n_symmetric_difference    | 0 |


  Scenario: Computing gaet metrics when the gff does not contain a product field
    Given I create a "cds/no_product" gff entry file "assembly.gff"
     When I run `gaet assembly.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
