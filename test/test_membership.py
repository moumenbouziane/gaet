import re
from gaet.types import Gene, GenePattern

import pytest

import gaet.gff                as gff
import gaet.util               as util
import gaet.metrics.membership as mem

PATTERN = GenePattern('Ala', re.compile('^tRNA-Ala\([atgc]{3}\)$'))

def test_is_membership_satisfied_with_no_matches():
    genes    = [Gene('', '', 'dummy', 10, 0)]
    products = [PATTERN]
    assert mem.is_membership_satisfied(genes, products) == False

def test_is_membership_satisfied_with_one_matches():
    genes    = [Gene('', '', 'tRNA-Ala(ggc)', 10, 0)]
    products = [PATTERN]
    assert mem.is_membership_satisfied(genes, products) == True

def test_is_membership_satisfied_with_some_matches():
    genes    = [Gene('', '', 'tRNA-Ala(ggc)', 10, 0)]
    products = [PATTERN, GenePattern('Arg', re.compile('^tRNA-Arg\([atgc]{3}\)$'))]
    assert mem.is_membership_satisfied(genes, products) == False

def test_is_membership_satisfied_with_all_matches():
    genes    = [Gene('', '', 'tRNA-Ala(ggc)', 10, 0), Gene('', '', 'tRNA-Arg(acg)', 10, 0)]
    products = [PATTERN, GenePattern('Arg', re.compile('^tRNA-Arg\([atgc]{3}\)$'))]
    assert mem.is_membership_satisfied(genes, products) == True

def test_membership_agreement():
    a = {'x' : True, 'y' : False}
    b = {'x' : True, 'y' : True}
    assert mem.membership_agreement(a, b) == {'x' : True, 'y' : False}
