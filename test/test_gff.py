import pytest, hashlib, tempfile
import zoo_helper as zoo
import gaet.gff  as gff

from gaet.types import *

def test_parse_normal_gff_file():
    gff_path = zoo.create_gff_file_from_zoo_entry('trna', 'alanine_ggc')
    genes = list(gff.parse_gff_into_genes(gff_path))
    assert len(genes) == 1
    digest = gff.create_digest('GGACAACACACTAACTCTGAAGCTGAGGTTCGACCACAATTACAACGTTATAGAAATCAA')
    gene = Gene('tRNA_1', 'tRNA', 'tRNA-Ala(ggc)', 60, digest)
    assert gene in genes


def test_parse_missing_product_gff_file():
    gff_path = zoo.create_gff_file_from_zoo_entry('cds', 'no_product')
    genes = list(gff.parse_gff_into_genes(gff_path))
    assert len(genes) == 1
    digest = gff.create_digest('ATGCATGCAT')
    gene = Gene('gene_1', 'CDS', 'hypothetical protein', 10, digest)
    assert gene in genes


def test_parse_repeat_region_gff_file():
    gff_path = zoo.create_gff_file_from_zoo_entry('repeat', 'simple')
    genes = list(gff.parse_gff_into_genes(gff_path))
    assert len(genes) == 1
    digest = gff.create_digest('ATGCATGCAT')
    gene = Gene('repeat_region_1', 'repeat_region', 'CRISPR', 10, digest)
    assert gene in genes


def test_gff_to_fasta():
    gff_path = zoo.create_gff_file_from_zoo_entry('trna', 'alanine_ggc')
    fasta = gff.gff_to_fasta(gff_path)
    assert fasta['Chromosome'][0:5].seq == 'GGACA'
