import re
import gaet.metrics.count as count

from gaet.types import GenePattern, Gene

PATTERN = GenePattern('Ala', re.compile('^tRNA-Ala\([atgc]{3}\)$'))

def test_count_product_matches_with_no_matching():
    genes = [Gene('', '', 'dummy', 10, 0)]
    assert count.count_gene_products(genes, PATTERN) == ('Ala', 0)

def test_count_product_matches_with_one_matching():
    genes = [Gene('', '', 'tRNA-Ala(ggc)', 10, 0)]
    assert count.count_gene_products(genes, PATTERN) == ('Ala', 1)

def test_count_product_matches_with_mixed_matching():
    genes = [Gene('', '', 'tRNA-Ala(ggc)', 10, 0), Gene('', '', 'dummy', 10, 0)]
    assert count.count_gene_products(genes, PATTERN) == ('Ala', 1)

def test_count_product_matches_with_multiple_matching():
    genes = [Gene('', '', 'tRNA-Ala(ggc)', 10, 0)] * 3
    assert count.count_gene_products(genes, PATTERN) == ('Ala', 3)
