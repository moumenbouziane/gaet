import pytest
import gaet.main as main
import gaet.gff  as gff

import zoo_helper as zoo

def test_assessing_partial_tRNA_annotations():
    results = main.simple_metrics(zoo.create_genes('trna', 'two_alanine_ggc'))

    metrics = ["sum_length", "count", "n50",  "l50"]

    # Gene group size metrics
    assert 'all'  in results['gene_type_size']
    assert 'tRNA' in results['gene_type_size']
    for m in metrics:
        assert m in results['gene_type_size']['tRNA']
        assert results['gene_type_size']['tRNA'][m] > 0

    # Gene copy size metrics
    assert 'ala_tRNA' in results['gene_copy_size']
    for m in metrics:
        assert m in results['gene_copy_size']['ala_tRNA']
        assert results['gene_copy_size']['ala_tRNA'][m] > 0

    assert 'tRNA' in results['minimum_gene_set']
    assert results['minimum_gene_set']['tRNA'] == False


def test_assessing_complete_tRNA_annotations():
    results = main.simple_metrics(zoo.create_genes('trna', 'complete_set'))
    assert 'tRNA' in results['minimum_gene_set']
    assert results['minimum_gene_set']['tRNA'] == True


def test_comparison_metrics():
    asm   = zoo.create_genes('trna', 'incomplete_set')
    ref   = zoo.create_genes('cds',  'two')

    results = main.comparison_metrics(asm, ref)
    assert 'gene_set_agreement' in results
    assert 'gene_type_distance' in results
    assert 'gene_type_size'     in results
    assert 'gene_copy_size'     in results

    distance = results['gene_type_distance']['all']
    assert distance['n_intersect'] == 0
    assert distance['n_symmetric_difference'] == 4
    assert distance['n_assembly_only'] == 2
    assert distance['n_reference_only'] == 2

    distance = results['gene_type_distance']['tRNA']
    assert distance['n_intersect'] == 0
    assert distance['n_symmetric_difference'] == 2
    assert distance['n_assembly_only'] == 2
    assert distance['n_reference_only'] == 0

    assert 'tRNA' in results['gene_type_size']
    assert 'all'  in results['gene_type_size']

    assert 'ala_tRNA' in results['gene_copy_size']
