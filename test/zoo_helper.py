import yaml
from functools import partial, reduce

def create_dummy_field(obj):
    """
    Create a dummy field name if one is not supplied
    """
    import hashlib
    field = "-".join(map(str, obj.items()))
    return hashlib.sha256(field.encode('utf-8')).hexdigest()[0:7]


def fill_in_missing_fields(entries):
    """
    If some fields are not defined, tries to create sensible defaults.
    """
    def _fill(entry):
        keys = ['ID', 'locus_tag', 'product']
        for k in keys:
            if k not in entry:
                entry[k] = create_dummy_field(entry)
        return entry
    return list(map(_fill, entries))


def get_entries_file(type_, name):
    import os.path
    dir_ = os.path.abspath(os.path.join(os.path.dirname(__file__), 'zoo'))
    return "/".join([dir_, type_, name]) + ".yml"


def validate_zoo_entries_seq_length(entries):
    """
    Ensure that the length of the sequence coordinates match the sequence string
    if it is supplied.
    """
    for e in entries:
        if 'seq' in e:
            if (e['end'] - e['start']) != len(e['seq'])-1:
                msg = "Start and end coords do not match for supplied seq length: {}"
                raise RuntimeError(msg.format(str(e)))


def validate_zoo_entries_id_uniqueness(entries):
    """
    Ensure that there are no duplicate gene IDs in the entries
    """
    ids = map(lambda x:  x['ID'], entries)
    # http://stackoverflow.com/a/9836685/91144
    seen = set()
    seen_add = seen.add
    seen_twice = set( x for x in ids if x in seen or seen_add(x) )
    if len(seen_twice) > 0:
        msg = "Duplicate GFF entries found: {}"
        raise RuntimeError(msg.format(seen_twice))


def validate_zoo_entries(entries):
    """
    Ensures that the zoo entries are valid
    """
    validate_zoo_entries_seq_length(entries)
    validate_zoo_entries_id_uniqueness(entries)


def chromosome_length(objs):
    """
    Calculates the length of the chromosome sequence that should be generated
    form the largest coordinate of entries
    """
    coords = reduce(lambda acc, x: acc + [x['start'], x['end']], objs, [])
    return max(list(coords))


def random_seq_generator(name, objs):
    """
    Generates a random FASTA sequence that is used in the '##FASTA' field in
    the generated GFF file.
    """
    import random
    length = chromosome_length(objs)
    sequence = "".join(random.sample(list('ATGC') * length, length))
    for o in objs:
        if 'seq' in o:
            # Update the generated FASTA if a specific sequence is given
            sequence = sequence[0:o['start']] + o['seq'] + sequence[o['end']:]
    return "> " + name + "\n" + sequence


def select_strand(obj):
    """
    Determines strand field in the GFF. Defaults to "+" if positive is not defined
    in the entry
    """
    if 'positive' in obj:
        return '+' if obj['positive'] else '-'
    else:
        return '+'




def create_metadata(obj):
    """
    Creates the 9th column metadata field in the GFF. Creates a dummy entry
    for a key if it does not exist in the given object.
    """
    keys = ['ID', 'locus_tag', 'product']
    metadata = []
    for k in keys:
        if k in obj and not obj[k]:
            # Do nothing if field is set to false
            None
        elif k in obj:
            metadata.append("=".join((k,obj[k])))
    if 'rpt_family' in obj:
            metadata.append("=".join(('rpt_family',obj['rpt_family'])))
    return ";".join(metadata)


def create_gff_string(chr_name, obj):
    """
    Create a GFF line entry based on a single zoo entry
    """
    fields = [chr_name, 'test_data', obj['type'], obj['start']+1, obj['end']+1, '.']
    fields.append(select_strand(obj))
    fields.append('.')
    fields.append(create_metadata(obj))
    return "\t".join(map(str, fields))


def create_gff_from_zoo_entry(type_, name):
    with open(get_entries_file(type_, name)) as f:
        entries = fill_in_missing_fields(yaml.load(f.read()))
    validate_zoo_entries(entries)

    chr_name    = "Chromosome"
    chromosome  = random_seq_generator(chr_name, entries)
    gff_entries = "\n".join(map(partial(create_gff_string, chr_name), entries))

    output = [
            "##gff-version 3",
            "##sequence-region {} 1 {}".format(chr_name, chromosome_length(entries) + 1),
            gff_entries,
            "##FASTA",
            chromosome]
    return "\n".join(output)


def create_gff_file_from_zoo_entry(type_, name):
    import hashlib, os.path
    with open(get_entries_file(type_, name)) as f:
        file_digest = hashlib.sha256(f.read().encode('utf-8')).hexdigest()
    path = os.path.join('tmp/zoo_cache', file_digest) + '.gff'
    if not os.path.exists(path):
        with open(path, 'w') as f:
            f.write(create_gff_from_zoo_entry(type_, name))
    return path


def create_genes(type_, name):
    import gaet.gff as gff
    gff_path = create_gff_file_from_zoo_entry(type_, name)
    return list(gff.parse_gff_into_genes(gff_path))

if __name__ == "__main__":
    import sys
    type_ = sys.argv[1]
    name  = sys.argv[2]
    print(create_gff_file_from_zoo_entry(type_, name))
