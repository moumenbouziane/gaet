import re

import zoo_helper as zoo
import gaet.gene  as gene

from gaet.types import *

ala_pattern = GenePattern('ala_tRNA', re.compile('^tRNA-Ala\([atgc]{3}\)(?:\s\(partial\))?$'))
arg_pattern = GenePattern('arg_tRNA', re.compile('^tRNA-Arg\([atgc]{3}\)(?:\s\(partial\))?$'))

def test_partition_genes_by_product_with_no_genes():
    assert gene.partition_genes_by_product([], []) == {}


def test_partition_genes_by_product_with_no_matches():
    assert gene.partition_genes_by_product([ala_pattern], []) == {'ala_tRNA' : []}


def test_partition_genes_by_product_with_for_trna_products():
    genes = list(zoo.create_genes('trna', 'incomplete_set'))
    partitions = gene.partition_genes_by_product(set([ala_pattern, arg_pattern]), genes)
    assert 'ala_tRNA' in partitions
    assert len(partitions['ala_tRNA']) == 1
    assert 'arg_tRNA' in partitions
    assert len(partitions['arg_tRNA']) == 1
